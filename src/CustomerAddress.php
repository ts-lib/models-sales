<?php

namespace TsLib\ModelsSales;

use Illuminate\Database\Eloquent\Model;

class CustomerAddress extends Model
{
    protected $table = "customer_address";

    protected $fillable = [
        'id',
        'customer_id',
        'default_shipping',
        'default_billing',
        'country',
        'addressee',
        'phone',
        'external_number',
        'internal_number',
        'street',
        'suburb',
        'town',
        'city',
        'state',
        'zip',
        'text',
        'between_strets',
        'suburb_id_efx',
        'town_id_efx',
        'city_id_efx',
        'state_id_efx',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
