<?php

namespace TsLib\ModelsSales;

use Illuminate\Database\Eloquent\Model;

class CustomerConfig extends Model
{
    protected $table = "customer_config";
    public $timestamps = false;

    protected $fillable = [
        "customer_id",
        "company_logo",
        "enduser_margin",
        "quote_duedate",
        "quote_deliverytime",
        "quote_condition",
        "quote_restriction",
        "quote_comment"
    ];
}
