<?php

namespace TsLib\ModelsSales;

use Illuminate\Database\Eloquent\Model;

class ApiCustomerTokens extends Model
{
    protected $table = "api_customer_tokens";
}
