<?php

namespace TsLib\ModelsSales;

use Illuminate\Database\Eloquent\Model;

class CustomerPricingGroup extends Model
{
    protected $table = "customer_pricing_group";

    protected $fillable = [
        "customer_id",
        "pricing_group_id",
        "pricing_group",
        "pricing_level_id",
        "pricing_level"
    ];
}
