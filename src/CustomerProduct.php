<?php

namespace TsLib\ModelsSales;

use Illuminate\Database\Eloquent\Model;

class CustomerProduct extends Model
{
    protected $table = "customer_product";

    protected $fillable = [
        "customer_id",
        "item_id",
        "currency",
        "price_level_id",
        "price_level",
        "price"
    ];
}
