<?php

namespace TsLib\ModelsSales;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $table = "contact";

    protected $fillable = [
        "id",
        "user_id",
        "customer_id",
        "phone",
        "margin",
        "birthday",
        "mobilephone",
        "jobtitle",
        "contactrole",
        "main_contact",
        "cige_decisor_com",
        "cige_decisor_tec",
    ];

    /*
    public function customer()
    {
        return $this->belongsTo('TsLib\ModelsSales\Customer');
    }

    
    public function user()
    {
        return $this->belongsTo('TsLib\ModelsGeneral\User');
    }
    */

    public function getMainContactAcAttribute()
    {
        return ($this->main_contact) ? 'Si' : '';
    }
}
