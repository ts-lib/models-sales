<?php

namespace TsLib\ModelsSales;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = "customer";

    protected $fillable = [
        "id",
        "user_id",
        "phone",
        "rfc",
        "sales_rep_id",
        "currency",
        "pricelevel",
        "default_shipping",
        "api_access",
        "api_key",
        "balance_MXN",
        "overdue_balance_MXN",
        "deposit_balance_MXN",
        "balance_USD",
        "overdue_balance_USD",
        "deposit_balance_USD",
        "clasificacion_potencial",
        "bd_asignado",
        "config",
        "free_local_shipping",
        "clasificacion_ventas",
        "cfdi_forma",
        "cfdi_method",
        "cfdi_uso",
        "cfdi_regimen",
        "category",
        "nivel_experiencia",
        "PO_required",
        "register_date",
        "cige_conoce",
        "cige_integra",
    ];
/*
    public function salesRep()
    {
        return $this->belongsTo('App\CustomerSalesRep');
    }
*/
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function addresses()
    {
        return $this->hasMany('App\CustomerAddress');
    }

    public function contacts()
    {
        return $this->hasMany('App\Contact');
    }

    public function getMainContactAttribute()
    {
        return $this->contacts->where('main_contact', 1)->first();
    }
/*
    public function CFDIForma()
    {
        return $this->hasOne('App\CFDIList', 'id_type', 'cfdi_forma');
    }

    public function CFDIMetodo()
    {
        return $this->hasOne('App\CFDIList', 'id_type', 'cfdi_method');
    }

    public function CFDIUso()
    {
        return $this->hasOne('App\CFDIList', 'id_type', 'cfdi_uso');
    }
*/
    public function getClasificacionShippingAttribute()
    {
        switch ($this->default_shipping) {
            case 1:
                return "Envio";
                break;
            case 2:
                return "Will Call";
                break;
            case 3:
                return "Envio a otra sucursal";
                break;
            case 4:
                return "Envio guia del cliente";
                break;
            case 5:
                return "Sin surtido preferido";
                break;
            case 6:
                return "Refacturacion";
                break;
            default:
                return "";
                break;
        }
    }

    public function getClasificacionVentasTextAttribute()
    {
        switch ($this->clasificacion_ventas) {
            case 1:
                return "Top";
                break;
            case 2:
                return "Activo";
                break;
            case 3:
                return "Congelado";
                break;
            case 4:
                return "Nuevo";
                break;
            default:
                return "";
                break;
        }
    }
/*
    public function getCFDIFormaNameAttribute()
    {
        if($this->CFDIForma == null)
            return '';
        $cfdi = $this->CFDIForma->where('type', '1')->where('id_type', $this->cfdi_forma)->first();
        if($cfdi != null)
            return $cfdi->name_type;
        else
            return '';
    }

    public function getCFDIMetodoNameAttribute()
    {
        if($this->CFDIMetodo == null)
            return '';
        $cfdi = $this->CFDIMetodo->where('type', '2')->where('id_type', $this->cfdi_method)->first();
        if($cfdi != null)
            return $cfdi->name_type;
        else
            return '';
    }

    public function getCFDIUsoNameAttribute()
    {
        if($this->CFDIUso == null)
            return '';
        $cfdi = $this->CFDIUso->where('type', '3')->where('id_type', $this->cfdi_uso)->first();
        if($cfdi != null)
            return $cfdi->name_type;
        else
            return '';
    }
*/
    public function getDireccionFiscalAttribute()
    {
        $address = $this->addresses->firstWhere('default_shipping', 1);

        if($address != null)
            return $address->text;
        else
            return '';
    }

    public function getEmailAttribute($value)
    {
        return utf8_encode(str_replace(["'", '"'], '', $value));
    }

    public function getMainContactNameAttribute()
    {
        $mc = $this->main_contact;
        if($mc != null && $mc->user != null)
            return $mc->user->name;
        else
            return '';
    }

    public function getZendeskNameAttribute()
    {
        return $this->user->code.' '.$this->user->name.' ('.$this->clasificacion_ventas_text.')(Nivel:'.$this->nivel_experiencia.') - Tel: '.$this->phone.' - Cont: '.$this->main_contact_name.' - Agente: '.$this->salesRep->name;
    }
}
