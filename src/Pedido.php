<?php

namespace TsLib\ModelsSales;

use Illuminate\Database\Eloquent\Model;

class Pedido extends Model
{
    //
    protected $table = "pending_sale_order";

    protected $fillable = [
        'customer_id',
        'customer_code',
        'user_id',
        'location_id',
        'delivery',
        'method_payment',
        'option_payment',
        'type_payment',
        'digits',
        'cfdi_forma',
        'cfdi_method',
        'cfdi_uso',
        'paypal_code',
        'paypal_amount',
        'currency',
        'comments',
        'zip',
        'address_id',
        'amount_shipping',
        'data_shipping',
        'data_items',
        'userFinal',
        'nameUserFinal',
        'phoneUserFinal',
        'startUserFinal',
        'endUserFinal',
        'documentId',
        'NoOrderCompra',
        'FileOrderOc',
        'quote',
        'status',
    ];

    public function getFolioAttribute()
    {
        return str_pad($this->id, 6, "0", STR_PAD_LEFT);
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
